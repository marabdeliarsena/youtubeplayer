using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V4.App;

namespace YouTubePlayer.Plugins.PlayerNotificationPanel
{
    public class NotificationPanel
    {

        private Context parent;
        private NotificationManager nManager;
        private NotificationCompat.Builder nBuilder;
        private RemoteViews remoteView;
        private PlayerBroadcastReciever reciever;

        public NotificationPanel(Context parent)
        {
            // TODO Auto-generated constructor stub
            this.parent = parent;
            nBuilder = new NotificationCompat.Builder(parent)
            .SetContentTitle("YouTube Player")
            .SetSmallIcon(Resource.Drawable.appIcon)
            .SetOngoing(true);

            remoteView = new RemoteViews(parent.PackageName, Resource.Layout.PlayerControlPanel);

            //set the button listeners
            SetListeners(remoteView);
            nBuilder.SetContent(remoteView);

            nManager = (NotificationManager)parent.GetSystemService(Context.NotificationService);
            nManager.Notify(2, nBuilder.Build());
        }

        public void SetListeners(RemoteViews view)
        {
            Intent previous = new Intent("playerBroadcast");
            previous.PutExtra("DO", (int)PlayerCommand.Previous);
            PendingIntent btn1 = PendingIntent.GetBroadcast(parent, 100, previous, 0);
            view.SetOnClickPendingIntent(Resource.Id.btnPrevious, btn1);

            //Intent pause = new Intent("playerBroadcast");
            //pause.PutExtra("DO", (int)PlayerCommand.Pause);
            //PendingIntent btn2 = PendingIntent.GetActivity(parent, 0, pause, 0);
            //view.SetOnClickPendingIntent(Resource.Id.btnPa, btn2);

            Intent play = new Intent("playerBroadcast");
            play.PutExtra("DO", (int)PlayerCommand.Play);
            PendingIntent btn3 = PendingIntent.GetBroadcast(parent, 100, play, 0);
            view.SetOnClickPendingIntent(Resource.Id.btnPlay, btn3);

            Intent next = new Intent("playerBroadcast");
            next.PutExtra("DO", (int)PlayerCommand.Next);
            PendingIntent btn4 = PendingIntent.GetBroadcast(parent, 100, next, 0);
            view.SetOnClickPendingIntent(Resource.Id.btnNext, btn4);
        }

        public void NotificationCancel()
        {
            nManager.Cancel(2);
        }
    }
}