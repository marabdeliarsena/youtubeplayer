using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Webkit;

namespace YouTubePlayer.Plugins.PlayerNotificationPanel
{
    [BroadcastReceiver]
    [IntentFilter(new[]{"playerBroadcast"})]
    public class PlayerBroadcastReciever : BroadcastReceiver
    {
        private WebView _webView;

        public PlayerBroadcastReciever()
        {

        }

        public override void OnReceive(Context context, Intent intent)
        {
            var activity = (YoutubeActivity)context;
            string url = "";
            PlayerCommand cmd = (PlayerCommand)intent.GetIntExtra("DO", -1);
            switch (cmd)
            {
                case PlayerCommand.Previous:
                    url = "Previous";
                    break;
                case PlayerCommand.Next:
                    url = "Next";
                    break;
                case PlayerCommand.Play:
                    url = "Play";
                    break;
                case PlayerCommand.Pause:
                    url = "Pause";
                    break;
                default:
                    break;
            }
            activity.ExecuteJavascript(url);
        }
    }
}