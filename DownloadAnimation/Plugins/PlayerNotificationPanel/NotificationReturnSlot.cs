using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;

namespace YouTubePlayer.Plugins.PlayerNotificationPanel
{
    [Activity(Label = "NotificationReturnSlot")]
    public class NotificationReturnSlot : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            string action = Intent.GetStringExtra("DO");
            if (action.Equals("volume"))
            {
                Log.Info("NotificationReturnSlot", "volume");
                //Your code
            }
            else if (action.Equals("stopNotification"))
            {
                //Your code
                Log.Info("NotificationReturnSlot", "stopNotification");
            }
            Finish();
        }
    }
}
