﻿namespace YouTubePlayer.YoutubeExtractor
{
    public enum AdaptiveType
    {
        None,
        Audio,
        Video
    }
}