using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Webkit;

namespace YouTubePlayer.Plugins
{
    public class VideoEnabledWebChromeClient : WebChromeClient, Android.Media.MediaPlayer.IOnPreparedListener, Android.Media.MediaPlayer.IOnCompletionListener, Android.Media.MediaPlayer.IOnErrorListener
    {

        public interface IToggledFullscreenCallback
        {
            void ToggledFullscreen(bool fullscreen);
        }

        private View activityNonVideoView;
        private ViewGroup activityVideoView;
        private View loadingView;
        private WebView webView;

        private bool isVideoFullscreen; // Indicates if the video is being displayed using a custom view (typically full-screen)
        private FrameLayout videoViewContainer;
        private ICustomViewCallback videoViewCallback;

        private IToggledFullscreenCallback toggledFullscreenCallback;

        /**
         * Never use this constructor alone.
         * This constructor allows this class to be defined as an inline inner class in which the user can override methods
         */
        [Java.Lang.SuppressWarnings]
        public VideoEnabledWebChromeClient()
        {
        }

        /**
         * Builds a video enabled WebChromeClient.
         * @param activityNonVideoView A View in the activity's layout that contains every other view that should be hidden when the video goes full-screen.
         * @param activityVideoView A ViewGroup in the activity's layout that will display the video. Typically you would like this to fill the whole layout.
         */
        [Java.Lang.SuppressWarnings]
        public VideoEnabledWebChromeClient(View activityNonVideoView, ViewGroup activityVideoView)
        {
            this.activityNonVideoView = activityNonVideoView;
            this.activityVideoView = activityVideoView;
            this.loadingView = null;
            this.webView = null;
            this.isVideoFullscreen = false;
        }

        /**
         * Builds a video enabled WebChromeClient.
         * @param activityNonVideoView A View in the activity's layout that contains every other view that should be hidden when the video goes full-screen.
         * @param activityVideoView A ViewGroup in the activity's layout that will display the video. Typically you would like this to fill the whole layout.
         * @param loadingView A View to be shown while the video is loading (typically only used in API level <11). Must be already inflated and without a parent view.
         */
        [Java.Lang.SuppressWarnings]
        public VideoEnabledWebChromeClient(View activityNonVideoView, ViewGroup activityVideoView, View loadingView)
        {
            this.activityNonVideoView = activityNonVideoView;
            this.activityVideoView = activityVideoView;
            this.loadingView = loadingView;
            this.webView = null;
            this.isVideoFullscreen = false;
        }

        /**
         * Builds a video enabled WebChromeClient.
         * @param activityNonVideoView A View in the activity's layout that contains every other view that should be hidden when the video goes full-screen.
         * @param activityVideoView A ViewGroup in the activity's layout that will display the video. Typically you would like this to fill the whole layout.
         * @param loadingView A View to be shown while the video is loading (typically only used in API level <11). Must be already inflated and without a parent view.
         * @param webView The owner VideoEnabledWebView. Passing it will enable the VideoEnabledWebChromeClient to detect the HTML5 video ended event and exit full-screen.
         * Note: The web page must only contain one video tag in order for the HTML5 video ended event to work. This could be improved if needed (see Javascript code).
         */
        public VideoEnabledWebChromeClient(View activityNonVideoView, ViewGroup activityVideoView, View loadingView, WebView webView)
        {
            this.activityNonVideoView = activityNonVideoView;
            this.activityVideoView = activityVideoView;
            this.loadingView = loadingView;
            this.webView = webView;
            this.isVideoFullscreen = false;
        }

        /**
         * Indicates if the video is being displayed using a custom view (typically full-screen)
         * @return true it the video is being displayed using a custom view (typically full-screen)
         */
        public bool IsVideoFullscreen()
        {
            return isVideoFullscreen;
        }

        /**
         * Set a callback that will be fired when the video starts or finishes displaying using a custom view (typically full-screen)
         * @param callback A VideoEnabledWebChromeClient.ToggledFullscreenCallback callback
         */
        public void SetOnToggledFullscreen(IToggledFullscreenCallback callback)
        {
            this.toggledFullscreenCallback = callback;
        }

        public override void OnShowCustomView(View view, WebChromeClient.ICustomViewCallback callback)
        {
            if (view is FrameLayout)
            {
                // A video wants to be shown
                FrameLayout frameLayout = (FrameLayout)view;
                View focusedChild = frameLayout.FocusedChild;

                // Save video related variables
                this.isVideoFullscreen = true;
                this.videoViewContainer = frameLayout;
                this.videoViewCallback = callback;

                // Hide the non-video view, add the video view, and show it
                activityNonVideoView.Visibility = ViewStates.Invisible;
                activityVideoView.AddView(videoViewContainer, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MatchParent, RelativeLayout.LayoutParams.MatchParent));
                activityVideoView.Visibility = ViewStates.Visible;

                if (focusedChild is Android.Widget.VideoView)
                {
                    // android.widget.VideoView (typically API level <11)
                    Android.Widget.VideoView videoView = (Android.Widget.VideoView)focusedChild;

                    // Handle all the required events
                    videoView.SetOnPreparedListener(this);
                    videoView.SetOnCompletionListener(this);
                    videoView.SetOnErrorListener(this);
                }
                else
                {
                    // Other classes, including:
                    // - android.webkit.HTML5VideoFullScreen$VideoSurfaceView, which inherits from android.view.SurfaceView (typically API level 11-18)
                    // - android.webkit.HTML5VideoFullScreen$VideoTextureView, which inherits from android.view.TextureView (typically API level 11-18)
                    // - com.android.org.chromium.content.browser.ContentVideoView$VideoSurfaceView, which inherits from android.view.SurfaceView (typically API level 19+)

                    // Handle HTML5 video ended event only if the class is a SurfaceView
                    // Test case: TextureView of Sony Xperia T API level 16 doesn't work fullscreen when loading the javascript below
                    if (webView != null && webView.Settings.JavaScriptEnabled && focusedChild is SurfaceView)
                    {
                        // Run javascript code that detects the video end and notifies the Javascript interface
                        String js = "javascript:";
                        js += "var _ytrp_html5_video_last;";
                        js += "var _ytrp_html5_video = document.getElementsByTagName('video')[0];";
                        js += "if (_ytrp_html5_video != undefined && _ytrp_html5_video != _ytrp_html5_video_last) {";
                        {
                            js += "_ytrp_html5_video_last = _ytrp_html5_video;";
                            js += "function _ytrp_html5_video_ended() {";
                            {
                                js += "_VideoEnabledWebView.notifyVideoEnd();"; // Must match Javascript interface name and method of VideoEnableWebView
                            }
                            js += "}";
                            js += "_ytrp_html5_video.addEventListener('ended', _ytrp_html5_video_ended);";
                        }
                        js += "}";
                        webView.LoadUrl(js);
                    }
                }

                // Notify full-screen change
                if (toggledFullscreenCallback != null)
                {
                    toggledFullscreenCallback.ToggledFullscreen(true);
                }
            }
        }

        public override void OnShowCustomView(View view, Android.Content.PM.ScreenOrientation requestedOrientation, WebChromeClient.ICustomViewCallback callback)
        {
            OnShowCustomView(view, callback);
        }

        public override void OnHideCustomView()
        {
            // This method should be manually called on video end in all cases because it's not always called automatically.
            // This method must be manually called on back key press (from this class' onBackPressed() method).

            if (isVideoFullscreen)
            {
                // Hide the video view, remove it, and show the non-video view
                activityVideoView.Visibility = ViewStates.Invisible;
                activityVideoView.RemoveView(videoViewContainer);
                activityNonVideoView.Visibility = ViewStates.Visible;

                // Call back (only in API level <19, because in API level 19+ with chromium webview it crashes)
                if (videoViewCallback != null && !videoViewCallback.GetType().Name.Contains(".chromium."))
                {
                    videoViewCallback.OnCustomViewHidden();
                }

                // Reset video related variables
                isVideoFullscreen = false;
                videoViewContainer = null;
                videoViewCallback = null;

                // Notify full-screen change
                if (toggledFullscreenCallback != null)
                {
                    toggledFullscreenCallback.ToggledFullscreen(false);
                }
            }
        }

        public override View VideoLoadingProgressView
        {
            get
            {
                if (loadingView != null)
                {
                    loadingView.Visibility = ViewStates.Visible;
                    return loadingView;
                }
                else
                {
                    return base.VideoLoadingProgressView;
                }
            }
        }

        /**
         * Notifies the class that the back key has been pressed by the user.
         * This must be called from the Activity's onBackPressed(), and if it returns false, the activity itself should handle it. Otherwise don't do anything.
         * @return Returns true if the event was handled, and false if was not (video view is not visible)
         */
        public bool OnBackPressed()
        {
            if (isVideoFullscreen)
            {
                OnHideCustomView();
                return true;
            }
            else
            {
                return false;
            }
        }

        public void OnPrepared(Android.Media.MediaPlayer mp)
        {
            if (loadingView != null)
            {
                loadingView.Visibility = ViewStates.Gone;
            }
        }
        
        public void OnCompletion(Android.Media.MediaPlayer mp)
        {
            OnHideCustomView();
        }

        public bool OnError(Android.Media.MediaPlayer mp, Android.Media.MediaError what, int extra)
        {
            return false;
        }

        
    }
}