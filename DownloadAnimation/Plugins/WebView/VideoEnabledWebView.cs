using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Webkit;
using Android.Annotation;
using Android.Util;

namespace YouTubePlayer.Plugins
{
    [Register("YouTubePlayer.Plugins.VideoEnabledWebView")]
    public class VideoEnabledWebView : WebView
    {
        public VideoEnabledWebChromeClient videoEnabledWebChromeClient;
        public bool addedJavascriptInterface;
        public class JavascriptInterface : Java.Lang.Object
        {
            VideoEnabledWebChromeClient videoEnabledWebChromeClient;
            bool addedJavascriptInterface;

            public JavascriptInterface(VideoEnabledWebChromeClient videoEnabledWebChromeClient, bool addedJavascriptInterface)
            {
                this.videoEnabledWebChromeClient = videoEnabledWebChromeClient;
                this.addedJavascriptInterface = addedJavascriptInterface;
            }

            [Android.Webkit.JavascriptInterface]
            public void notifyVideoEnd() // Must match Javascript interface method of VideoEnabledWebChromeClient
            {
                // This code is not executed in the UI thread, so we must force that to happen
                new Handler(Looper.MainLooper).Post(new Java.Lang.Runnable(delegate
                {

                    if (this.videoEnabledWebChromeClient != null)
                    {
                        this.videoEnabledWebChromeClient.OnHideCustomView();
                    }
                }));
            }
        }

        public VideoEnabledWebView(Context context)
            : base(context)
        {

            addedJavascriptInterface = false;
        }

        [Java.Lang.SuppressWarnings]
        public VideoEnabledWebView(Context context, IAttributeSet attrs)
            : base(context, attrs)
        {

            addedJavascriptInterface = false;
        }

        [Java.Lang.SuppressWarnings]
        public VideoEnabledWebView(Context context, IAttributeSet attrs, int defStyle)
            : base(context, attrs, defStyle)
        {
            ;
            addedJavascriptInterface = false;
        }

        /**
         * Indicates if the video is being displayed using a custom view (typically full-screen)
         * @return true it the video is being displayed using a custom view (typically full-screen)
         */
        public bool IsVideoFullscreen()
        {
            return videoEnabledWebChromeClient != null && videoEnabledWebChromeClient.IsVideoFullscreen();
        }

        /**
         * Pass only a VideoEnabledWebChromeClient instance.
         */
//        [SuppressLint]
        public override void SetWebChromeClient(WebChromeClient client)
        {
            Settings.JavaScriptEnabled = (true);

            if (client is VideoEnabledWebChromeClient)
            {
                this.videoEnabledWebChromeClient = (VideoEnabledWebChromeClient)client;
            }

            base.SetWebChromeClient(client);
        }

        public override void LoadData(string data, string mimeType, string encoding)
        {
            AddJavascriptInterface();
            base.LoadData(data, mimeType, encoding);
        }

        public override void LoadDataWithBaseURL(string baseUrl, string data, string mimeType, string encoding, string historyUrl)
        {
            AddJavascriptInterface();
            base.LoadDataWithBaseURL(baseUrl, data, mimeType, encoding, historyUrl);
        }
        public override void LoadUrl(string url)
        {
            AddJavascriptInterface();
            base.LoadUrl(url);
        }
        public override void LoadUrl(string url, IDictionary<string, string> additionalHttpHeaders)
        {
            AddJavascriptInterface();
            base.LoadUrl(url, additionalHttpHeaders);
        }

        private void AddJavascriptInterface()
        {
            if (!addedJavascriptInterface)
            {
                // Add javascript interface to be called when the video ends (must be done before page load)
                AddJavascriptInterface(new JavascriptInterface(videoEnabledWebChromeClient,addedJavascriptInterface), "_VideoEnabledWebView"); // Must match Javascript interface name of VideoEnabledWebChromeClient

                addedJavascriptInterface = true;
            }
        }

    }
}