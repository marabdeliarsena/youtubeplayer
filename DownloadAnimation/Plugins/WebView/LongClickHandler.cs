﻿using System;
using Android.OS;

namespace YouTubePlayer
{
    public class LongClickHandler : Handler
    {
        IOnLongClickHandleCallBack _handleCallBack;

        public LongClickHandler(IOnLongClickHandleCallBack handleCallback) : base()
        {
            _handleCallBack = handleCallback;
        }

        public override void HandleMessage(Message msg)
        {
            string url = (string)msg.Data.Get("url");
            if (url != null)
                _handleCallBack.LongClickHandled(url);
        }
    }

    public interface IOnLongClickHandleCallBack
    {
        void LongClickHandled(string url) ;
    }
}

