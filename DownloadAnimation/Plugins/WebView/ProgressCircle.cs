//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//using Android.App;
//using Android.Content;
//using Android.OS;
//using Android.Runtime;
//using Android.Views;
//using Android.Widget;
//using Android.Graphics;
//using Android.Animation;
//using Android.Util;
//using Android.Content.Res;
//using Android.Views.Animations;

//namespace YouTubePlayer.Plugins
//{
//    class ProgressCircle : View
//    {

//        private static float INDETERMINANT_MIN_SWEEP = 15f;

//        private Paint paint;
//        private int size = 0;
//        private RectF bounds;

//        private bool isIndeterminate, autostartAnimation;
//        private float currentProgress, maxProgress, indeterminateSweep, indeterminateRotateOffset;
//        private int thickness, color, animDuration, animSwoopDuration, animSyncDuration, animSteps;

//        private List<CircularProgressViewListener> listeners;
//        // Animation related stuff
//        private float startAngle;
//        private float actualProgress;
//        private ValueAnimator startAngleRotate;
//        private ValueAnimator progressAnimator;
//        private AnimatorSet indeterminateAnimator;

//        public ProgressCircle(Context context)
//            : base(context)
//        {
//            init(null, 0);
//        }

//        public ProgressCircle(Context context, IAttributeSet attrs)
//            : base(context, attrs)
//        {

//            init(attrs, 0);
//        }

//        public ProgressCircle(Context context, IAttributeSet attrs, int defStyle)
//            : base(context, attrs, defStyle)
//        {

//            init(attrs, defStyle);
//        }

//        protected void init(IAttributeSet attrs, int defStyle)
//        {
//            listeners = new ArrayList<CircularProgressViewListener>();

//            initAttributes(attrs, defStyle);

//            paint = new Paint(PaintFlags.AntiAlias);
//            updatePaint();

//            bounds = new RectF();
//        }

//        private void initAttributes(IAttributeSet attrs, int defStyle)
//    {
//        TypedArray a = Context.ObtainStyledAttributes(attrs, Resource.styleable.CircularProgressView, defStyle, 0);

//        Resources resources = Resources;

//        // Initialize attributes from styleable attributes
//        currentProgress = a.GetFloat(Resource.styleable.CircularProgressView_cpv_progress,
//                resources.GetInteger(Resource.integeResource.cpv_default_progress));
//        maxProgress = a.GetFloat(Resource.styleable.CircularProgressView_cpv_maxProgress,
//                resources.GetInteger(Resource.integeResource.cpv_default_max_progress));
//        thickness = a.GetDimensionPixelSize(Resource.styleable.CircularProgressView_cpv_thickness,
//                resources.GetDimensionPixelSize(Resource.dimen.cpv_default_thickness));
//        isIndeterminate = a.Getbool(Resource.styleable.CircularProgressView_cpv_indeterminate,
//                resources.Getbool(Resource.bool.cpv_default_is_indeterminate));
//        autostartAnimation = a.Getbool(Resource.styleable.CircularProgressView_cpv_animAutostart,
//                resources.Getbool(Resource.bool.cpv_default_anim_autostart));

//        int accentColor = Context.Resources.GetIdentifier("colorAccent", "attr", Context.PackageName);
            
//        // If color explicitly provided
//        if (a.HasValue(Resource.styleable.CircularProgressView_cpv_color)) {
//            color = a.GetColor(Resource.styleable.CircularProgressView_cpv_color, resources.GetColor(Resource.coloResource.cpv_default_color));
//        }
//        // If using support library v7 accentColor
//        else if(accentColor != 0) {
//            TypedValue t = new TypedValue();
//            Context.Theme.ResolveAttribute(accentColor, t, true);
//            color = t.Data;
//        }
//        // If using native accentColor (SDK >21)
//        else if (Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.Lollipop) {
//            TypedArray t = Context.ObtainStyledAttributes(new int[] { Android.Resource.attResource.colorAccent });
//            color = t.GetColor(0, resources.GetColor(Resource.coloResource.cpv_default_color));
//        }
//        else {
//            //Use default color
//            color = resources.GetColor(Resource.coloResource.cpv_default_color);
//        }

//        animDuration = a.GetInteger(Resource.styleable.CircularProgressView_cpv_animDuration,
//                resources.GetInteger(Resource.integeResource.cpv_default_anim_duration));
//        animSwoopDuration = a.GetInteger(Resource.styleable.CircularProgressView_cpv_animSwoopDuration,
//                resources.GetInteger(Resource.integeResource.cpv_default_anim_swoop_duration));
//        animSyncDuration = a.GetInteger(Resource.styleable.CircularProgressView_cpv_animSyncDuration,
//                resources.GetInteger(Resource.integeResource.cpv_default_anim_sync_duration));
//        animSteps = a.GetInteger(Resource.styleable.CircularProgressView_cpv_animSteps,
//                resources.GetInteger(Resource.integeResource.cpv_default_anim_steps));
//        a.Recycle();
//    }

//        protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec)
//{
//     base.OnMeasure(widthMeasureSpec, heightMeasureSpec);
//        base.OnMeasure(widthMeasureSpec, heightMeasureSpec);
//        int xPad = PaddingLeft + PaddingRight;
//        int yPad = PaddingTop + PaddingBottom;
//        int width = MeasuredWidth - xPad;
//        int height = MeasuredHeight - yPad;
//        size = (width < height) ? width : height;
//        SetMeasuredDimension(size + xPad, size + yPad);
//}
//    protected override void OnSizeChanged(int w, int h, int oldw, int oldh) {
//        base.OnSizeChanged(w, h, oldw, oldh);
//        size = (w < h) ? w : h;
//        updateBounds();
//    }

//    private void updateBounds()
//    {
//        int paddingLeft = getPaddingLeft();
//        int paddingTop = getPaddingTop();
//        bounds.set(paddingLeft + thickness, paddingTop + thickness, size - paddingLeft - thickness, size - paddingTop - thickness);
//    }

//    private void updatePaint()
//    {
//        paint.setColor(color);
//        paint.SetStyle(Paint.Style.Stroke);
//        paint.StrokeWidth = thickness;
//        paint.StrokeCap = Paint.Cap.Butt;
//    }
    
//    protected override void OnDraw(Canvas canvas) {
//        base.OnDraw(canvas);

//        // Draw the arc
//        float sweepAngle = IsInEditMode ? currentProgress/maxProgress*360 : actualProgress/maxProgress*360;
//        if(!isIndeterminate)
//            canvas.DrawArc(bounds, startAngle, sweepAngle, false, paint);
//        else
//            canvas.DrawArc(bounds, startAngle + indeterminateRotateOffset, indeterminateSweep, false, paint);
//    }

//    /**
//     * Returns the mode of this view (determinate or indeterminate).
//     * @return true if this view is in indeterminate mode.
//     */
//    public bool isIndeterminate() {
//        return isIndeterminate;
//    }

//    /**
//     * Sets whether this CircularProgressView is indeterminate or not.
//     * It will reset the animation if the mode has changed.
//     * @param isIndeterminate True if indeterminate.
//     */
//    public void setIndeterminate(bool isIndeterminate) {
//        bool old = this.isIndeterminate;
//        bool reset = this.isIndeterminate == isIndeterminate;
//        this.isIndeterminate = isIndeterminate;
//        if (reset)
//            resetAnimation();
//        if(old != isIndeterminate) {
//            for(CircularProgressViewListener listener : listeners) {
//                listener.onModeChanged(isIndeterminate);
//            }
//        }
//    }

//    /**
//     * Get the thickness of the progress bar arc.
//     * @return the thickness of the progress bar arc
//     */
//    public int getThickness() {
//        return thickness;
//    }

//    /**
//     * Sets the thickness of the progress bar arc.
//     * @param thickness the thickness of the progress bar arc
//     */
//    public void setThickness(int thickness) {
//        this.thickness = thickness;
//        updatePaint();
//        updateBounds();
//        Invalidate();
//    }

//    /**
//     *
//     * @return the color of the progress bar
//     */
//    public int getColor() {
//        return color;
//    }

//    /**
//     * Sets the color of the progress bar.
//     * @param color the color of the progress bar
//     */
//    public void setColor(int color) {
//        this.color = color;
//        updatePaint();
//        Invalidate();
//    }

//    /**
//     * Gets the progress value considered to be 100% of the progress bar.
//     * @return the maximum progress
//     */
//    public float getMaxProgress() {
//        return maxProgress;
//    }

//    /**
//     * Sets the progress value considered to be 100% of the progress bar.
//     * @param maxProgress the maximum progress
//     */
//    public void setMaxProgress(float maxProgress) {
//        this.maxProgress = maxProgress;
//        Invalidate();
//    }

//    /**
//     * @return current progress
//     */
//    public float getProgress() {
//        return currentProgress;
//    }

//    /**
//     * Sets the progress of the progress bar.
//     *
//     * @param currentProgress the new progress.
//     */
//    public void setProgress(float currentProgress) {
//        this.currentProgress = currentProgress;
//        // Reset the determinate animation to approach the new currentProgress
//        if (!isIndeterminate) {
//            if (progressAnimator != null && progressAnimator.isRunning())
//                progressAnimator.cancel();
//            progressAnimator = ValueAnimator.ofFloat(actualProgress, currentProgress);
//            progressAnimator.SetDuration(animSyncDuration);
//            progressAnimator.SetInterpolator(new LinearInterpolator());
//            progressAnimator.AddUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//                override
//                public void onAnimationUpdate(ValueAnimator animation) {
//                    actualProgress = (Float) animation.getAnimatedValue();
//                    Invalidate();
//                }
//            });
//            progressAnimator.addListener(new AnimatorListenerAdapter() {
//                override
//                public void onAnimationEnd(Animator animation) {
//                    for(CircularProgressViewListener listener : listeners) {
//                        listener.onProgressUpdateEnd(currentProgress);
//                    }
//                }
//            });

//            progressAnimator.start();
//        }
//        Invalidate();
//        for(CircularProgressViewListener listener : listeners) {
//            listener.onProgressUpdate(currentProgress);
//        }
//    }

//    /**
//     * Register a CircularProgressViewListener with this View
//     * @param listener The listener to register
//     */
//    public void addListener(CircularProgressViewListener listener) {
//        if(listener != null)
//            listeners.add(listener);
//    }

//    /**
//     * Unregister a CircularProgressViewListener with this View
//     * @param listener The listener to unregister
//     */
//    public void removeListener(CircularProgressViewListener listener) {
//        listeners.remove(listener);
//    }

//    /**
//     * Starts the progress bar animation.
//     * (This is an alias of resetAnimation() so it does the same thing.)
//     */
//    public void startAnimation() {
//        resetAnimation();
//    }

//    /**
//     * Resets the animation.
//     */
//    public void resetAnimation() {
//        // Cancel all the old animators
//        if(startAngleRotate != null && startAngleRotate.isRunning())
//            startAngleRotate.cancel();
//        if(progressAnimator != null && progressAnimator.isRunning())
//            progressAnimator.cancel();
//        if(indeterminateAnimator != null && indeterminateAnimator.isRunning())
//            indeterminateAnimator.cancel();

//        // Determinate animation
//        if(!isIndeterminate)
//        {
//            // The cool 360 swoop animation at the start of the animation
//            startAngle = -90f;
//            startAngleRotate = ValueAnimator.ofFloat(-90f, 270f);
//            startAngleRotate.setDuration(animSwoopDuration);
//            startAngleRotate.setInterpolator(new DecelerateInterpolator(2));
//            startAngleRotate.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//                override
//                public void onAnimationUpdate(ValueAnimator animation) {
//                    startAngle = (Float) animation.getAnimatedValue();
//                    Invalidate();
//                }
//            });
//            startAngleRotate.start();

//            // The linear animation shown when progress is updated
//            actualProgress = 0f;
//            progressAnimator = ValueAnimator.ofFloat(actualProgress, currentProgress);
//            progressAnimator.setDuration(animSyncDuration);
//            progressAnimator.setInterpolator(new LinearInterpolator());
//            progressAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//                override
//                public void onAnimationUpdate(ValueAnimator animation) {
//                    actualProgress = (Float) animation.getAnimatedValue();
//                    Invalidate();
//                }
//            });
//            progressAnimator.start();
//        }
//        // Indeterminate animation
//        else
//        {
//            startAngle = -90f;
//            indeterminateSweep = INDETERMINANT_MIN_SWEEP;
//            // Build the whole AnimatorSet
//            indeterminateAnimator = new AnimatorSet();
//            AnimatorSet prevSet = null, nextSet;
//            for(int k=0;k<animSteps;k++)
//            {
//                nextSet = createIndeterminateAnimator(k);
//                AnimatorSet.Builder builder = indeterminateAnimator.play(nextSet);
//                if(prevSet != null)
//                    builder.after(prevSet);
//                prevSet = nextSet;
//            }

//            // Listen to end of animation so we can infinitely loop
//            indeterminateAnimator.addListener(new AnimatorListenerAdapter() {
//                bool wasCancelled = false;
//                override
//                public void onAnimationCancel(Animator animation) {
//                    wasCancelled = true;
//                }

//                override
//                public void onAnimationEnd(Animator animation) {
//                    if(!wasCancelled)
//                        resetAnimation();
//                }
//            });
//            indeterminateAnimator.start();
//            for(CircularProgressViewListener listener : listeners) {
//                listener.onAnimationReset();
//            }
//        }


//    }

//    // Creates the animators for one step of the animation
//    private AnimatorSet createIndeterminateAnimator(float step)
//    {
//        final float maxSweep = 360f*(animSteps-1)/animSteps + INDETERMINANT_MIN_SWEEP;
//        final float start = -90f + step*(maxSweep-INDETERMINANT_MIN_SWEEP);

//        // Extending the front of the arc
//        ValueAnimator frontEndExtend = ValueAnimator.ofFloat(INDETERMINANT_MIN_SWEEP, maxSweep);
//        frontEndExtend.setDuration(animDuration/animSteps/2);
//        frontEndExtend.setInterpolator(new DecelerateInterpolator(1));
//        frontEndExtend.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//            override
//            public void onAnimationUpdate(ValueAnimator animation) {
//                indeterminateSweep = (Float) animation.getAnimatedValue();
//                Invalidate();
//            }
//        });

//        // Overall rotation
//        ValueAnimator rotateAnimator1 = ValueAnimator.ofFloat(step*720f/animSteps, (step+.5f)*720f/animSteps);
//        rotateAnimator1.setDuration(animDuration/animSteps/2);
//        rotateAnimator1.setInterpolator(new LinearInterpolator());
//        rotateAnimator1.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//            override
//            public void onAnimationUpdate(ValueAnimator animation) {
//            indeterminateRotateOffset = (Float) animation.getAnimatedValue();
//            }
//        });

//        // Followed by...

//        // Retracting the back end of the arc
//        ValueAnimator backEndRetract = ValueAnimator.ofFloat(start, start+maxSweep-INDETERMINANT_MIN_SWEEP);
//        backEndRetract.setDuration(animDuration/animSteps/2);
//        backEndRetract.setInterpolator(new DecelerateInterpolator(1));
//        backEndRetract.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//            override
//            public void onAnimationUpdate(ValueAnimator animation) {
//            startAngle = (Float) animation.getAnimatedValue();
//            indeterminateSweep = maxSweep - startAngle + start;
//            Invalidate();
//            }
//        });

//        // More overall rotation
//        ValueAnimator rotateAnimator2 = ValueAnimator.ofFloat((step + .5f) * 720f / animSteps, (step + 1) * 720f / animSteps);
//        rotateAnimator2.setDuration(animDuration / animSteps / 2);
//        rotateAnimator2.setInterpolator(new LinearInterpolator());
//        rotateAnimator2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//            override
//            public void onAnimationUpdate(ValueAnimator animation) {
//                indeterminateRotateOffset = (Float) animation.getAnimatedValue();
//            }
//        });

//        AnimatorSet set = new AnimatorSet();
//        set.play(frontEndExtend).with(rotateAnimator1);
//        set.play(backEndRetract).with(rotateAnimator2).after(rotateAnimator1);
//        return set;
//    }

//    override
//    protected void onAttachedToWindow() {
//        super.onAttachedToWindow();
//        if(autostartAnimation)
//            startAnimation();
//    }

//    override protected void onDetachedFromWindow() {
//        super.onDetachedFromWindow();
//        if(startAngleRotate != null) {
//            startAngleRotate.cancel();
//            startAngleRotate = null;
//        }
//        if(progressAnimator != null) {
//            progressAnimator.cancel();
//            progressAnimator = null;
//        }
//        if(indeterminateAnimator != null) {
//            indeterminateAnimator.cancel();
//            indeterminateAnimator = null;
//        }
//    }
//    }
//}