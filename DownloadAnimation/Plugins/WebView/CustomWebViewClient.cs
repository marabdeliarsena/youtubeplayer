using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Webkit;

namespace YouTubePlayer.Plugins
{
    public class CustomWebViewClient : WebViewClient 
    {
        bool loadingFinished = true;
        bool redirect = false;

        IOnLoadingFinishedCallBack _loadingCallBack;

        public CustomWebViewClient(IOnLoadingFinishedCallBack loadingCallBack)
        {
            _loadingCallBack = loadingCallBack;
        }

        public CustomWebViewClient(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference,transfer)
        {

        }

        public override void OnLoadResource(WebView view, string url)
        {
            base.OnLoadResource(view, url);
        }

        public override bool ShouldOverrideUrlLoading(WebView view, string url)
        {
            if (!loadingFinished)
            {
                redirect = true;
            }
            loadingFinished = false;

            return false; // base.ShouldOverrideUrlLoading(view, url);
        }

        public override void OnPageStarted(WebView view, string url, Android.Graphics.Bitmap favicon)
        {
            loadingFinished = false;
            base.OnPageStarted(view, url, favicon);
        }

        public override void OnPageFinished(WebView view, string url)
        {
            base.OnPageFinished(view, url);

            if (!redirect)
            {
                loadingFinished = true;
            }

            if (loadingFinished && !redirect)
            {
                _loadingCallBack.LoadingFinished();
            }
            else
            {
                redirect = false; 
            }
        }
    }

    public interface IOnLoadingFinishedCallBack{
        void LoadingFinished();
    }
}