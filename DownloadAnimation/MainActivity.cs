﻿//using System;
//using Android.App;
//using Android.Content;
//using Android.Runtime;
//using Android.Views;
//using Android.Widget;
//using Android.OS;
//using Android.Views.Animations;
//using Android.Util;
//using Android.Animation;

//namespace YouTubePlayer
//{
//    [Activity(Label = "YouTubePlayer", MainLauncher = false, Icon = "@drawable/appIcon")]
//    public class MainActivity : Activity, View.IOnTouchListener , Animation.IAnimationListener
//    {
//        int count = 1;
//        bool toogle = false;
//        private int _xDelta;
//        private int _yDelta;
//        Button button;
//        Button btn;
//        View mRootView;
//        TranslateAnimation anim;
//        Animation animBounce;
//        ImageView arrow;

//        protected override void OnCreate(Bundle bundle)
//        {
//            base.OnCreate(bundle);

//            // Set our view from the "main" layout resource
//            SetContentView(Resource.Layout.Main);

//            // Get our button from the layout resource,
//            // and attach an event to it
//            mRootView = FindViewById(Android.Resource.Id.Content).RootView;
//            button = FindViewById<Button>(Resource.Id.MyButton);
//            button.Click += delegate
//            {
//                Intent intent = new Intent(this, typeof(YoutubeActivity));
//                StartActivity(intent);
//            };
//            //button.SetOnTouchListener(this);
//            btn = FindViewById<Button>(Resource.Id.button1);
//            btn.SetOnTouchListener(this);



//            arrow = FindViewById<ImageView>(Resource.Id.arrow);
//            #region YouTubePlayer
//            DisplayMetrics metrics = new DisplayMetrics();
//            WindowManager.DefaultDisplay.GetMetrics(metrics);
//            switch (metrics.DensityDpi)
//            {
//                case DisplayMetricsDensity.D280:
//                    anim = new TranslateAnimation(0, 0, 0, 20);
//                    break;
//                case DisplayMetricsDensity.D400:
//                    anim = new TranslateAnimation(0, 0, 0, 40);
//                    break;
//                case DisplayMetricsDensity.D560:
//                    anim = new TranslateAnimation(0, 0, 0, 50);
//                    break;
//                case DisplayMetricsDensity.High:
//                    anim = new TranslateAnimation(0, 0, 0, 20);
//                    break;
//                case DisplayMetricsDensity.Medium:
//                    anim = new TranslateAnimation(0, 0, 0, 20);
//                    break;
//                case DisplayMetricsDensity.Xhigh:
//                    anim = new TranslateAnimation(0, 0, 0, 25);
//                    break;
//                case DisplayMetricsDensity.Xxhigh:
//                    anim = new TranslateAnimation(0, 0, 0, 45);
//                    break;
//                case DisplayMetricsDensity.Xxxhigh:
//                    anim = new TranslateAnimation(0, 0, 0, 50);
//                    break;
//                default:
//                    anim = new TranslateAnimation(0, 0, 0, 20);
//                    break;
//            }

//            anim.Duration = 500;
//            anim.RepeatCount = Android.Views.Animations.Animation.Infinite;
//            anim.FillAfter = true;
//            anim.RepeatMode = RepeatMode.Reverse;
//            anim.SetInterpolator(this, Android.Resource.Animation.LinearInterpolator);


//            button.Click += (o, e) =>
//            {
//                RunOnUiThread(() =>
//                {
//                    toogle = !toogle;
//                    if (toogle)
//                    {
//                        //progressBar.Visibility = ViewStates.Visible;
//                        //progressBar.Enabled = true;
//                        arrow.StartAnimation(anim);
//                    }
//                    else
//                    {
//                        //progressBar.Visibility = ViewStates.Gone;
//                        //progressBar.Enabled = false;
//                        arrow.ClearAnimation();
//                    }
//                });
//            };
//            #endregion
//        }

//        int length;
//        public bool OnTouch(View v, MotionEvent e)
//        {
//            int X = (int)e.RawX;
//            int Y = (int)e.RawY;
//            switch (e.Action)
//            {
//                case MotionEventActions.Down:
//                    RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams)v.LayoutParameters;
//                    _xDelta = X - lParams.LeftMargin;
//                    _yDelta = Y - lParams.TopMargin;
//                    break;
//                case MotionEventActions.Up:

//                    length = mRootView.Height - Y * 2;
//                    animBounce = new TranslateAnimation(0, 0, 0, mRootView.Height - Y*2);
//                    animBounce.Duration = 1000;
//                    //animBounce.RepeatCount = 1;
//                    animBounce.FillAfter = false;
//                    //animBounce.RepeatMode = RepeatMode.Reverse;
//                    animBounce.SetInterpolator(this, Android.Resource.Animation.BounceInterpolator);
//                    animBounce.SetAnimationListener(this);
//                    btn.StartAnimation(animBounce);


//                    //toogle = !toogle;
//                    //if (toogle)
//                    //{
//                    //    //progressBar.Visibility = ViewStates.Visible;
//                    //    //progressBar.Enabled = true;
//                    //    arrow.StartAnimation(anim);
//                    //}
//                    //else
//                    //{
//                    //    //progressBar.Visibility = ViewStates.Gone;
//                    //    //progressBar.Enabled = false;
//                    //    arrow.ClearAnimation();
//                    //}
//                    break;
//                case MotionEventActions.PointerDown:
//                    break;
//                case MotionEventActions.PointerUp:
//                    break;
//                case MotionEventActions.Move:
//                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)v.LayoutParameters;
//                    layoutParams.LeftMargin = X - _xDelta;
//                    layoutParams.TopMargin = Y - _yDelta;
//                    layoutParams.RightMargin = -250;
//                    layoutParams.BottomMargin = -250;
//                    v.LayoutParameters = layoutParams;
//                    break;
//            }
//            mRootView.Invalidate();
//            return true;
//        }

//        public void OnAnimationEnd(Animation animation)
//        {
//            var parameters = (RelativeLayout.LayoutParams)btn.LayoutParameters;
//            parameters.TopMargin += length;
//            btn.LayoutParameters = parameters;
//        }

//        public void OnAnimationRepeat(Animation animation)
//        {
//            //throw new NotImplementedException();
//        }

//        public void OnAnimationStart(Animation animation)
//        {
//            //throw new NotImplementedException();
//        }
//    }
//}

