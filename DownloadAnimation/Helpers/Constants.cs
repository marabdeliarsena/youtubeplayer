﻿using System;
using System.Reflection;

namespace YouTubePlayer
{
    public static class Constants
    {
        public static string Directorypath
        {
            get
            {
                return System.IO.Path.Combine((string)Android.OS.Environment.ExternalStorageDirectory,
                    (string)Assembly.GetExecutingAssembly().GetName().Name);
            }
        }
    }
}

