using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Webkit;
using YouTubePlayer.Plugins;
using YouTubePlayer.Plugins.PlayerNotificationPanel;
using Android.Support.V4.Content;
using YouTubePlayer.YoutubeExtractor;
using System.IO;
using YoutubeExtractor;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using com.refractored.fab;
using Android.Content.PM;



namespace YouTubePlayer
{
    [Activity(Label = "YouTube Player", MainLauncher = true, Icon = "@drawable/appIcon", ScreenOrientation = ScreenOrientation.Portrait)]
    [IntentFilter (new[]{Intent.ActionView},Categories=new[]{Intent.CategoryDefault, Intent.CategoryBrowsable}, DataScheme="http", DataHost="www.youtube.com", DataPathPrefix="")]
    [IntentFilter (new[]{Intent.ActionView},Categories=new[]{Intent.CategoryDefault, Intent.CategoryBrowsable}, DataScheme="http", DataHost="youtube.com", DataPathPrefix="")]
    [IntentFilter (new[]{Intent.ActionView},Categories=new[]{Intent.CategoryDefault, Intent.CategoryBrowsable}, DataScheme="http", DataHost="m.youtube.com", DataPathPrefix="")]
    [IntentFilter (new[]{Intent.ActionView},Categories=new[]{Intent.CategoryDefault, Intent.CategoryBrowsable}, DataScheme="http", DataHost="www.m.youtube.com", DataPathPrefix="")]
    [IntentFilter (new[]{Intent.ActionView},Categories=new[]{Intent.CategoryDefault, Intent.CategoryBrowsable}, DataScheme="http", DataHost="youtu.be", DataPathPrefix="")]
    [IntentFilter (new[]{Intent.ActionView},Categories=new[]{Intent.CategoryDefault, Intent.CategoryBrowsable}, DataScheme="http", DataHost="www.youtu.be", DataPathPrefix="")]
    [IntentFilter (new[]{Intent.ActionView},Categories=new[]{Intent.CategoryDefault, Intent.CategoryBrowsable}, DataScheme="https", DataHost="www.youtube.com", DataPathPrefix="")]
    [IntentFilter (new[]{Intent.ActionView},Categories=new[]{Intent.CategoryDefault, Intent.CategoryBrowsable}, DataScheme="https", DataHost="youtube.com", DataPathPrefix="")]
    [IntentFilter (new[]{Intent.ActionView},Categories=new[]{Intent.CategoryDefault, Intent.CategoryBrowsable}, DataScheme="https", DataHost="m.youtube.com", DataPathPrefix="")]
    [IntentFilter (new[]{Intent.ActionView},Categories=new[]{Intent.CategoryDefault, Intent.CategoryBrowsable}, DataScheme="https", DataHost="www.m.youtube.com", DataPathPrefix="")]
    [IntentFilter (new[]{Intent.ActionView},Categories=new[]{Intent.CategoryDefault, Intent.CategoryBrowsable}, DataScheme="https", DataHost="youtu.be", DataPathPrefix="")]
    [IntentFilter (new[]{Intent.ActionView},Categories=new[]{Intent.CategoryDefault, Intent.CategoryBrowsable}, DataScheme="https", DataHost="www.youtu.be", DataPathPrefix="")]
//    [IntentFilter(new[]{ Intent.ActionView }, Categories = new[]{ Intent.CategoryDefault, Intent.CategoryBrowsable }
//        , DataSchemes = new[]{"http", "https"}
//        , DataHosts = new []{"youtube.com", "www.youtube.com", "m.youtube.com", "www.m.youtube.com", "youtu.be", "www.youtu.be"
//                            ,"youtube.com", "www.youtube.com", "m.youtube.com", "www.m.youtube.com", "youtu.be", "www.youtu.be"})]
    
    public class YoutubeActivity : Activity, VideoEnabledWebChromeClient.IToggledFullscreenCallback, IOnLongClickHandleCallBack, IOnLoadingFinishedCallBack
    {
        public WebView _webView;
        private Handler _longClickHandler;
        private VideoEnabledWebChromeClient webChromeClient;
        private ConnectivityPlugin _cp;
        private NotificationPanel _nPanel;

        private BroadcastReceiver mMessageReceiver;

        FloatingActionButton _btnDownload;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            
            _cp = new ConnectivityPlugin();
            CheckNetwork();
        }

        void CheckNetwork()
        {
            if (_cp.IsNetworkReachable)
            {
                InitWebView();
            }
            else
            {
                InitErrorView();
            }
        }

        bool IsVideoUrl(string url, string originalUrl)
        {            
            return originalUrl.Contains("m.youtube.com/watch?v=") || url.Contains("m.youtube.com/watch?v=")
            || originalUrl.Contains("m.youtube.com/watch?list=")
            || url.Contains("m.youtube.com/watch?list=");
            
        }

        void InitErrorView()
        {
            SetContentView(Resource.Layout.ErrorLayout);
            var btn = FindViewById<Button>(Resource.Id.retry);
            btn.Click += delegate
            {
                CheckNetwork();
            };
        }

        void InitWebView()
        {
            SetContentView(Resource.Layout.layout1);

            _webView = FindViewById<WebView>(Resource.Id.webView);
            View nonVideoLayout = FindViewById<RelativeLayout>(Resource.Id.nonVideoLayout); // Your own view, read class comments
            RelativeLayout videoLayout = FindViewById<RelativeLayout>(Resource.Id.videoLayout); // Your own view, read class comments
            View loadingView = LayoutInflater.Inflate(Resource.Layout.LoadingLayout, null); // Your own view, read class comments

            _webView.Settings.JavaScriptEnabled = true;
            webChromeClient = new VideoEnabledWebChromeClient(nonVideoLayout, videoLayout, loadingView, _webView);
            webChromeClient.SetOnToggledFullscreen(this);
            _webView.SetWebChromeClient(webChromeClient);
            _webView.SetWebViewClient(new CustomWebViewClient(this));
//            _webView.SetWebViewClient(new WebViewClient());
            _webView.LongClick += OnWebViewLoncClick;

            Android.Net.Uri data = Intent.Data;
            if (data != null)
            {
                string normalizedUrl = "";
                DownloadUrlResolver.TryNormalizeYoutubeUrl(data.ToString(), out normalizedUrl);
                _webView.LoadUrl(normalizedUrl);
            }
            else
            {
                _webView.LoadUrl("http://m.youtube.com");
            }


            _btnDownload = FindViewById<FloatingActionButton>(Resource.Id.fab);

            _longClickHandler = new LongClickHandler(this);

            mMessageReceiver = new PlayerBroadcastReciever();
            LocalBroadcastManager.GetInstance(this).RegisterReceiver(mMessageReceiver, new IntentFilter("playerBroadcast"));
        }

        void OnWebViewLoncClick(object sender, View.LongClickEventArgs e)
        {
            var result = _webView.GetHitTestResult();
            if (result.Type == HitTestResult.SrcAnchorType)
            {
                Message msg = _longClickHandler.ObtainMessage();
                _webView.RequestFocusNodeHref(msg);
            }
        }

        public void LongClickHandled(string url)
        {
            Intent sendIntent = new Intent();
            sendIntent.SetAction(Intent.ActionSend);
            sendIntent.PutExtra(Intent.ExtraText, url);
            sendIntent.SetType("text/plain");
            StartActivity(Intent.CreateChooser(sendIntent, "Share Link"));
        }

        public async void LoadingFinished()
        {
            Task taskToWait = Task.Delay(500);
            await taskToWait;
            if (IsVideoUrl(_webView.Url, _webView.OriginalUrl))
            {
                _btnDownload.Visibility = ViewStates.Visible;
                _btnDownload.Click += DownloadAudioClicked;                        
            }
            else
            {
                _btnDownload.Visibility = ViewStates.Invisible;
            }
        }

        async void DownloadAudioClicked(object sender, EventArgs e)
        {
            ProgressDialog progress;
            progress = ProgressDialog.Show(this, null, "Preparing Download");
            List<VideoInfo> videoInfos = await DownloadUrlResolver.GetDownloadUrlsAsync(_webView.OriginalUrl);
            progress.Dismiss();

            progress = ProgressDialog.Show(this, null, "Downloading Audio");
            await Task.Run(() =>
                {
                    DownloadAudio(videoInfos);
                });
                
            progress.Dismiss();   
            Toast.MakeText(this, "Congrats! Your Music Is Saved In \"/sdcard/YoutubePlayer\" Folder", ToastLength.Long).Show();
        }

        private async Task DownloadAudio(List<VideoInfo> videoInfos)
        {
            VideoInfo video = videoInfos
                                    .Where(info => info.CanExtractAudio)
                                    .OrderByDescending(info => info.AudioBitrate)
                                    .First();

            if (video.RequiresDecryption)
            {
                DownloadUrlResolver.DecryptDownloadUrl(video);
            }

            if (!Directory.Exists(Constants.Directorypath))
            {
                Directory.CreateDirectory(Constants.Directorypath);
            }
                
            var audioDownloader = new AudioDownloader(video, Path.Combine(Constants.Directorypath, RemoveIllegalPathCharacters(video.Title) + video.AudioExtension));
            bool started = false;
            audioDownloader.DownloadProgressChanged += (object sender, ProgressEventArgs e) =>
            {
                if (!started)
                {
                    Console.WriteLine(string.Format("**** Downloading: {0}\n", video.Title));
                    started = true;
                }
            };
            audioDownloader.DownloadFinished += (object sender, EventArgs e) =>
            {
                Console.WriteLine(string.Format("**** Finished Downloading: {0}\n", video.Title));
            };

            try
            {
                audioDownloader.Execute();
            }
            catch (Exception ex)
            {
                Console.WriteLine("fuck");
            }
        }

        private static string RemoveIllegalPathCharacters(string path)
        {
            string regexSearch = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
            var r = new Regex(string.Format("[{0}]", Regex.Escape(regexSearch)));
            return r.Replace(path, "");
        }

        public void ToggledFullscreen(bool fullscreen)
        {
            if (fullscreen)
            {
                WindowManagerLayoutParams attrs = Window.Attributes;
                attrs.Flags |= WindowManagerFlags.Fullscreen;
                attrs.Flags |= WindowManagerFlags.KeepScreenOn;
                Window.Attributes = (attrs);
                if ((int)Android.OS.Build.VERSION.SdkInt >= 14)
                {
                    Window.DecorView.SystemUiVisibility = StatusBarVisibility.Hidden;
                }
            }
            else
            {
                WindowManagerLayoutParams attrs = Window.Attributes;
                attrs.Flags &= WindowManagerFlags.Fullscreen;
                attrs.Flags &= WindowManagerFlags.KeepScreenOn;
                Window.Attributes = (attrs);
                if ((int)Android.OS.Build.VERSION.SdkInt >= 14)
                {
                    Window.DecorView.SystemUiVisibility = StatusBarVisibility.Visible;
                }
            }
        }

        public void ExecuteJavascript(string command)
        {
            if (_webView != null)
            {
                _webView.LoadUrl(command);
            }
        }

        #region Native Methods

        public override void OnBackPressed()
        {
            if (!webChromeClient.OnBackPressed())
            {
                if (_webView.CanGoBack())
                {
                    var backForwardList = _webView.CopyBackForwardList();
                    var prevPage = backForwardList.GetItemAtIndex(backForwardList.CurrentIndex - 1);
                    if (IsVideoUrl(prevPage.Url, prevPage.OriginalUrl))
                    {
                        _btnDownload.Visibility = ViewStates.Visible;
                    }
                    else
                    {
                        _btnDownload.Visibility = ViewStates.Invisible;
                    }
                    _webView.GoBack();
                }
                else
                {
                    // Close app (presumably)
                    base.OnBackPressed();
                }
            }
        }

        protected override void OnPause()
        {
            base.OnPause();
            //_nPanel = new NotificationPanel(this);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            //_nPanel.NotificationCancel();
            LocalBroadcastManager.GetInstance(this).UnregisterReceiver(mMessageReceiver);
        }

        #endregion
    }
}